
module Thursday.Convert
  ( convert_to_typed
  , gett
  ) where

import Thursday.Ast
import Thursday.Unique

gett :: TAst -> TypeLabel
gett (TLet t _ _ _) = t
gett (TApply t _ _) = t
gett (TFun t _ _) = t
gett (TVar t _) = t
gett (TInt t _) = t
gett (TStr t _) = t

convert_to_typed :: Ast -> IO TAst

convert_to_typed (Fun var body') = do
  body <- convert_to_typed body'
  typelabel <- newUnique
  var_typelabel <- newUnique
  return $ TFun typelabel (var, var_typelabel) body

convert_to_typed (Let var expr' body') = do
  expr <- convert_to_typed expr'
  body <- convert_to_typed body'
  typelabel <- newUnique
  return $ TLet typelabel var expr body

convert_to_typed (Apply f' a') = do
    f <- convert_to_typed f'
    a <- convert_to_typed a'
    typelabel <- newUnique
    return $ TApply typelabel f a

convert_to_typed (Var name) = do
    typelabel <- newUnique
    return $ TVar typelabel name

convert_to_typed (Int i) = do
    typelabel <- newUnique
    return $ TInt typelabel i

convert_to_typed (Str s) = do
    typelabel <- newUnique
    return $ TStr typelabel s

