
module Thursday.Parser
  ( parse
  ) where

import Thursday.Ast
import Text.ParserCombinators.ReadP
import Data.Char (isDigit, isAlpha)

parseWith :: ReadP a -> String -> a
parseWith p s = case [a | (a,"") <- readP_to_S p s] of
  [x] -> x
  []  -> error "no parse"
  _   -> error "ambiguous parse"

parse :: String -> Ast
parse = parseWith ast where
  ast, _let, _fun, _apply, _var, _string, _int :: ReadP Ast
  ast = choice [_fun, _var, _int, _string, _let, _apply]

  _let = do
    string "let"
    skipSpaces
    name <- munch isAlpha
    skipSpaces
    expr <- ast
    skipSpaces
    string "in" 
    skipSpaces
    body <- ast
    return $ Let name expr body
  
  _fun = do
    string "\\"
    name <- munch isAlpha
    skipSpaces
    body <- ast
    return $ Fun name body

  _apply = do
    string "("
    f <- ast
    skipSpaces
    a <- ast
    string ")"
    return $ Apply f a

  _var = do
    -- TOOD: make this more succinct
    name <- munch isAlpha
    return $ Var name

  _string = do
    char '"'
    text <- manyTill get (char '"')
    return $ Str text

  _int = do
    i_string <- munch isDigit
    return $ Int (read i_string)
