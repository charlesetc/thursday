
module Thursday.Ast
  ( Ast(..)
  , (-->)
  , (~~)
  , BaseType(..)
  , TAst(..)
  , TypeLabel
  , VarLabel
  ) where

type TypeLabel = Int
type VarLabel = String

-- map an ast to a type
data BaseType
  = StrType
  | IntType
  | UnitType
  deriving (Show, Eq)

data Ast
  = Fun VarLabel Ast
  | Let VarLabel Ast Ast
  | Apply Ast Ast
  | Var String
  | Int Integer
  | Str String
  deriving (Show, Eq, Ord)

data TAst
  = TFun TypeLabel (VarLabel, TypeLabel) TAst
  | TLet TypeLabel VarLabel TAst TAst
  | TApply TypeLabel TAst TAst
  | TVar TypeLabel String
  | TInt TypeLabel Integer
  | TStr TypeLabel String
  deriving (Show, Eq, Ord)

(-->) :: String -> Ast -> Ast
(-->) a b = Fun a b
(~~) :: Ast -> Ast -> Ast
(~~) a b = Apply a b

