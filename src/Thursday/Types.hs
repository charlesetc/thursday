{-# OPTIONS -Wall #-}

module Thursday.Types
    ( run_inference
    , Ast(..)
    , (-->)
    , (~~)
    ) where

import qualified Data.Map as M
import Data.Map ((!))
import Data.Maybe (maybe)

import Debug.Trace (trace)

import Thursday.Ast
import Thursday.Unique
import Thursday.Convert

type Substitution =  M.Map TypeLabel TypeLabel
type Graph =  M.Map TypeLabel MonoType
type Env = M.Map String PolyType

data MonoType
  = Generic TypeLabel
  | Free TypeLabel
  | Arrow TypeLabel TypeLabel
  | Base BaseType
  deriving (Show, Eq)

data PolyType
  = Mono MonoType
  | Poly TypeLabel
  deriving (Show, Eq)

run_inference :: Bool -> Ast -> IO ()
run_inference should_debug ast = do
  typed_ast  <- convert_to_typed ast
  graph      <- infer typed_ast 

  if should_debug then do
    putStrLn $ "ast:\t" ++ show typed_ast
    putStrLn $ "graph:\t" ++ show graph
    putStrLn $ "type:\t" ++ show_type (gett typed_ast) graph
  else
    putStrLn $ show_ast typed_ast graph

show_ast :: TAst -> Graph -> [Char]
show_ast (TFun ty (v, _) body) g =
  "(fun "
  ++ v
  ++ " => "
  ++ show_ast body g
  ++ "):[" ++ show_type ty g ++ "]"
show_ast (TLet ty name expr body) g =
  "(let "
  ++ name
  ++ " = "
  ++ show_ast expr g
  ++ " in "
  ++ show_ast body g
  ++ "):[" ++ show_type ty g ++ "]"
show_ast (TApply ty f a) g =
  "("
  ++ show_ast f g
  ++ " "
  ++ show_ast a g
  ++ ")"
  ++ ":[" ++ show_type ty g ++ "]"
show_ast a _ = show a

find :: Graph -> MonoType -> MonoType
find graph (Generic t) = maybe (Free t) (find graph) (M.lookup t graph)
find _ anything_else = anything_else

findl :: Graph -> TypeLabel -> MonoType
findl g = find g . Generic

show_type :: TypeLabel -> Graph -> String
show_type t g = case M.lookup t g of
  Just (Free t')-> "'" ++ show t' -- should be the same as t.
  Just (Generic t') -> show_type t' g
  Just (Arrow a b) -> show_type a g ++ " -> " ++ show_type b g
  Just (Base b) -> show b
  Nothing -> "'" ++ show t

new_substitution :: Graph -> TypeLabel -> IO Substitution
new_substitution g t = subst (Generic t) M.empty where
  subst (Generic t') s = do
    u <- newUnique
    s' <- subst (g ! t') s
    return $ M.insert t' u s'
  subst (Free t') s = newUnique >>= \u -> return $ M.insert t' u s
  subst (Base _) s    = return s
  subst (Arrow a b) s =
    subst (g ! a) s >>= \s' -> subst (g ! b) s'

clone_type :: Graph -> MonoType -> Substitution -> IO (Graph, TypeLabel)
-- clone_type g _ _ | trace ("clone: " ++ show g) False = undefined
clone_type g b@(Base _) _ = do
  u <- newUnique
  return (M.insert u b g, u)
clone_type g (Arrow a b) s = do
  u <- newUnique
  (g', a') <- clone_type g (findl g a) s
  (g'', b') <- clone_type g' (findl g' b) s
  let arrow = Arrow a' b'
  return (M.insert u arrow g'', u)
clone_type _ (Generic _) _ = undefined -- impossible with find
clone_type g (Free t) s = do
  let u = s ! t
  return (M.insert u (Free u) g, u)

infer :: TAst -> IO Graph
infer ast = reconstruct ast M.empty M.empty

reconstruct :: TAst -> Env -> Graph -> IO Graph

reconstruct (TInt t _) _ graph = return $
  unify (Generic t) (Base IntType) graph

reconstruct (TStr t _) _ graph = return $
  unify (Generic t) (Base StrType) graph

reconstruct (TVar t name) env graph = do
  case M.lookup name env of
    -- Do a normal unify if it's a function call:
    Just (Mono _type) -> return $ unify (Generic t) _type graph
    -- Duplicate the quantified variables and then unify
    -- if it's a polymorphic type.
    Just (Poly label) -> do
      substitution <- new_substitution graph label
      (graph', t') <- clone_type graph (findl graph label) substitution
      -- let (t', graph') = (t, graph)
      return $ unify (Generic t) (Generic t') graph'
    Nothing -> error ("variable " ++ name ++ " does not exist")

reconstruct (TApply t f a) env graph = do
  let graph' = unify (Generic $ gett f) (Arrow (gett a) t) graph
  graph'' <- reconstruct a env graph'
  reconstruct f env graph''

reconstruct (TLet t name expr body) env graph = do
  graph' <- reconstruct expr env graph
  graph'' <- reconstruct body (M.insert name (Poly (gett expr)) env) graph'
  return $ unify (Generic t) (Generic $ gett body) graph''

reconstruct (TFun t (name, tv) body) env graph = do
  graph' <- reconstruct body (M.insert name (Mono . Generic $ tv) env) graph
  let graph'' = M.insert tv (Free tv) graph'
  return $ unify (Generic t) (Arrow tv (gett body)) graph''

unify :: MonoType -> MonoType -> Graph -> Graph
-- unify x y _ | trace ("unified: " ++ show x ++ ", " ++ show y) False = undefined
unify x y firstgraph = unify' (find firstgraph x) (find firstgraph y) firstgraph where
  unify' (Generic _) _ _ = undefined -- find should never return Generic
  unify' _ (Generic _) _ = undefined -- find should never return Generic

  unify' (Base a) (Base b) graph = case a == b of
    True -> graph
    _ -> error $ "got base type " ++ show a ++ ", expected base type " ++ show b

  unify' a@(Base _) b@(Arrow _ _) graph = unify' b a graph
  unify' a@(Arrow _ _) b@(Base _) _ =  error $ "got base type " ++ show b ++ ", expected arrow type " ++ show a

  unify' (Arrow a b) (Arrow a' b') graph = graph' where
    graph' = unify (g b) (g b') $ unify (g a) (g a') $ graph
    g = Generic

  unify' a b graph | a == b = graph
  unify' (Free a) b graph = M.insert a b graph
  unify' a b@(Free _) graph = unify' b a graph