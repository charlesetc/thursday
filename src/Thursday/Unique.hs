
module Thursday.Unique
  ( newUnique
  ) where

import qualified Data.Unique as Unique
import qualified Data.Map as M
import Thursday.Ast

newUnique :: IO TypeLabel
newUnique = Unique.hashUnique <$> Unique.newUnique
