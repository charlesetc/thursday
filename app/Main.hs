module Main where

import Thursday.Ast -- (run)
import Thursday.Types (run_inference)
import Thursday.Parser (parse)
import System.Environment (getArgs)

import Data.List (lines)

-- ast :: Ast
-- ast =
--   Let "id"  ("x" --> Var "x") $
--     Var "id" ~~ Int 2

-- main = run_inference ast
main :: IO ()
main = do
  args <- getArgs
  input <- getContents
  sequence ( map (run_inference (args == ["debug"]) . parse) $ lines input)
  return ()

